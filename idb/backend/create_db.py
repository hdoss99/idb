#!/usr/bin/env python3

import json
from backend.models import app, db, Cat, Bird, Dog

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_cats/birds/dogs
# ------------
def create_cats():
    """
    populate cats table
    """
    cats = load_json('backend/cat_data.json')

    for oneCat in cats['animals']:
        if len(oneCat['photos']) > 0:
            id = oneCat['id']
            url = oneCat['url']
            name = oneCat['name']
            gender = oneCat['gender']
            color = oneCat['colors']['primary']
            img_url = oneCat['photos'][0]['medium']
            age = oneCat['age']
            breed = oneCat['breeds']['primary']

            newCat = Cat(id = id, url = url, name = name, gender = gender, color = color, img_url=img_url, age = age, breed = breed)

            # After I create the cat, I can then add it to my session. 
            db.session.add(newCat)
            # commit the session to my DB.
            db.session.commit()

def create_birds():
    """
    populate birds table
    """
    birds = load_json('backend/bird_data.json')

    for oneBird in birds['animals']:
        if len(oneBird['photos']) > 0:
            id = oneBird['id']
            url = oneBird['url']
            name = oneBird['name']
            gender = oneBird['gender']
            color = oneBird['colors']['primary']
            img_url = oneBird['photos'][0]['medium']
            age = oneBird['age']
            breed = oneBird['breeds']['primary']

            newBird = Bird(id = id, url = url, name = name, gender = gender, color = color, img_url=img_url, age = age, breed = breed)

            # After I create the bird, I can then add it to my session. 
            db.session.add(newBird)
            # commit the session to my DB.
            db.session.commit()

def create_dogs(): # DOUBLE CHECK PARAMETERS
    """
    populate dogs table
    """
    dogs = load_json('backend/dog_data.json')

    for oneDog in dogs['animals']:
        if len(oneDog['photos']) > 0:
            id = oneDog['id']
            url = oneDog['url']
            name = oneDog['name']
            gender = oneDog['gender']
            color = oneDog['colors']['primary']
            img_url = oneDog['photos'][0]['medium']
            age = oneDog['age']
            breed = oneDog['breeds']['primary']

            newDog = Dog(id = id, url = url, name = name, gender = gender, color = color, img_url=img_url, age = age, breed = breed)

            # After I create the dog, I can then add it to my session. 
            db.session.add(newDog)
            # commit the session to my DB.
            db.session.commit()


create_cats()
create_birds()
create_dogs()
