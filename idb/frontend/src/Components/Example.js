import React from 'react'

const Example = () => {
    return (
        <div>
            <p>This is the example component</p>
        </div>
    )
}

export default Example