import React from "react"
import * as ReactBootStrap from "react-bootstrap"

function Menu() {
    return (
        <div style={{borderBottom: "1px solid white"}}>
            <ReactBootStrap.Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <ReactBootStrap.Navbar.Brand href="/"> Find-a-Friend </ReactBootStrap.Navbar.Brand>
                <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
                    <ReactBootStrap.Nav className="mr-auto">
                    {/* <ReactBootStrap.NavDropdown title="Search by Species" id="collasible-nav-dropdown">
                        <ReactBootStrap.NavDropdown.Item href="/dogs"> Dogs </ReactBootStrap.NavDropdown.Item>
                        <ReactBootStrap.NavDropdown.Item href="/cats"> Cats </ReactBootStrap.NavDropdown.Item>
                        <ReactBootStrap.NavDropdown.Item href="/birds"> Birds </ReactBootStrap.NavDropdown.Item>
                    </ReactBootStrap.NavDropdown> */}
                    </ReactBootStrap.Nav>
                    <ReactBootStrap.Nav>
                    <ReactBootStrap.Nav.Link href="/about"> About </ReactBootStrap.Nav.Link>
                    {/* <ReactBootStrap.Nav.Link eventKey={2} href="#memes"> Login </ReactBootStrap.Nav.Link> */}
                    </ReactBootStrap.Nav>
                </ReactBootStrap.Navbar.Collapse>
            </ReactBootStrap.Navbar>
        </div>
    )
}

export default Menu