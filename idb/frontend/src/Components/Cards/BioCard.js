import React from "react"

function BioCard(props){
    return(
        <div style={{color: "white"}}>
        <h2>{props.person.name}</h2>
                <img src={props.person.imgSRC} alt="Imagehere"></img>
                <h3>Bio</h3>
                <p style={{ flexShrink: 1 }}>{props.person.bio}</p>
                <br/><br/>
                <h3>Responsibilities</h3>
                <p>{props.person.responsibilities}</p>
                <br/><br/>
                <p>No. of Issues: {props.person.numIssues}</p><br/>
                <p>No. of Commits: {props.person.numCommits}</p><br/>
                <p>No. of Unit Tests: {props.person.numUnitTests}</p>      
        </div>
    )

}

export default BioCard

{/* <div class='card-row'>
            <div class="card d-inline-flex float-left p-2 position-relative bg-dark text-white m-4 border-white">
                <h2>{props.person.name}</h2>
                <img src={props.person.imgSRC} alt="Imagehere"></img>
                <h3>Bio</h3>
                <p style={{ flexShrink: 1 }}>{props.person.bio}</p>
                <br/><br/>
                <h3>Responsibilities</h3>
                <p>{props.person.responsibilities}</p>
                <br/><br/>
                <p>No. of Issues: {props.person.numIssues}</p><br/>
                <p>No. of Commits: {props.person.numCommits}</p><br/>
                <p>No. of Unit Tests: {props.person.numUnitTests}</p>            

            </div>
        </div> */}