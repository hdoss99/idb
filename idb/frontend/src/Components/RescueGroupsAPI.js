import React, {useEffect,useState} from 'react'
import {Card,Button} from 'react-bootstrap'
import gremlin from "../Photos/gremlinDogAustin.jpeg"

const RescueGroupsAPI = () => {

    const API_Key = '4bANt44Q'
    
    const BASE_PATH = 'https://test1-api.rescuegroups.org/v5/public/orgs/'

    const headers = { 
        'Content-Type': 'application/json', 
        'Authorization': API_Key ,
    }

    const [info, setInfo] = useState()

    useEffect(() => {
        fetch(BASE_PATH, {
            method: 'GET',
            headers: headers,
        }) 
        .then(results => results.json())
        .then(data => {
            setInfo(data)
            console.log(data)
        })
    },[])

    return (
        <div>
            <Card style={{ width: '18rem' }}>
            {/* <Card.Img variant="top" src={gremlin} />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text style={{ color: '#000' }}>
                    Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                    </Card.Text>
                    <Button variant="primary">Go somewhere</Button>
                </Card.Body> */}
            </Card>
        </div>
    )
}

export default RescueGroupsAPI