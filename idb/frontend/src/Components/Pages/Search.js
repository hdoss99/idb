import React, { useEffect, useState } from 'react'
import { useForm } from "react-hook-form"
import Dogs from './Dogs'
import Cats from './Cats'
import Birds from './Birds'
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import '../../Styles/Search.css'

const Search = () => {
    const [species, setSpecies] = useState("")
    const [breed, setBreed] = useState("")
    const [color, setColor] = useState("")
    const [age, setAge] = useState("")
    const [gender, setGender] = useState("")

    const { register, handleSubmit } = useForm()

    const onSubmit = (filterData) => {
        setSpecies(filterData.species)
        setBreed(filterData.breed)
        setColor(filterData.color)
        setAge(filterData.age)
        setGender(filterData.gender)
        console.log(filterData)
    }

    const speciesSelect = () => {
        if(species == "dog"){
            return <Dogs breed={breed} color={color} age={age} gender={gender}/>
        }
        else if(species == "cat"){
            return <Cats breed={breed} color={color} age={age} gender={gender}/>
        }
        else if(species == "bird"){
            return <Birds breed={breed} color={color} age={age} gender={gender}/>
        }
    }

    useEffect(() => {
        speciesSelect()
    }, [species,breed,color,age,gender])

    return (
        <div className="searchBar">
            <form onSubmit={handleSubmit(onSubmit)} class="flex-form">
                <label for="search-bar-breed">Breed:</label>
                <input id="search-bar-breed" {...register("breed")} placeholder="Search by Breed"/>
                <br></br>
                <label for="search-bar-color">Color:</label>
                <input id="search-bar-color" {...register("color")} placeholder="Search by Color"/>
                <label for="species-select">Species:</label>
                <select id="species-select" {...register("species")}>
                    <option value="dog">Dogs</option>
                    <option value="cat">Cats</option>
                    <option value="bird">Birds</option>
                </select>
                <label for="age-select">Age:</label>
                <select id="age-select" {...register("age")}>
                    <option value="All">All</option>
                    <option value="Baby">Baby</option>
                    <option value="Young">Young</option>
                    <option value="Adult">Adult</option>
                    <option value="Senior">Senior</option>
                </select>
                <label for="gender-select">Gender:</label>
                <select id="gender-select" {...register("gender")}>
                    <option value="All">All</option>
                    <option value="Female">Female</option>
                    <option value="Male">Male</option>
                </select>
                <input type="submit" value="Search"/>
            </form>
            {speciesSelect()}
        </div>
    );
}
export default Search