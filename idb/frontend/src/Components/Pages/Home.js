import React from "react"
import { Link } from 'react-router-dom'
import Button from "react-bootstrap/Button"
import '../../Styles/Home.css'

function Home() {
    return(
        <div className="bg">
            <div id="content" >
                <h1>Because Everyone Deserves A Home.</h1>
                <Link to="/Search">
                    <Button variant="outline-light" size="lg">Find Your Friend Now</Button>
                </Link>
            </div>
        </div>
    )
}

export default Home;