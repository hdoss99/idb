import React from 'react'
import Menu from './Components/Menu'
import Home from './Components/Pages/Home'

import About from "./Components/Pages/About"
import dogTable from "./Components/Pages/Dogs"
import catTable from "./Components/Pages/Cats"
import birdTable from "./Components/Pages/Birds"
import Instance from "./Components/Pages/Instance"
import Search from "./Components/Pages/Search"
import './Styles/App.css';
import {Route, BrowserRouter as Router} from "react-router-dom"


// import RescueGroupsAPI from './Components/RescueGroupsAPI'
// import Example from './Components/Example'

function App() {

  return (
    // <Search/>
    <Router>
        <div className="App bg-dark">
          <Menu style={{border: "1px solid white"}}/>

        </div>
        <Route path={"/"} exact component={Home} />
        <Route path={"/search"} exact component={Search} />
        <Route path={"/about"} exact component={About} />
        <Route path={"/dogs"} exact component={dogTable} />
        <Route path={"/cats"} exact component={catTable}/>
        <Route path={"/birds"} exact component={birdTable}/>
        <Route path={"/:species/:id"} exact component={Instance}/>
    </Router>
  );
}

export default App;
